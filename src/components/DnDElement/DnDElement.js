import React, { Component } from 'react';
import { DragSource  } from 'react-dnd';
import enhanceWithClickOutside from 'react-click-outside';
import { 
    Popover,
    IconButton,
    TextField,
    InputAdornment 
} from 'material-ui';

import {
    FormatBold,
    FormatItalic,
    FormatSize
} from '@material-ui/icons';

import './DnDElement.sass';


const boxSource = {
    beginDrag(props) {
        const { id, left, top, fontSize } = props;
        return { id, left, top, fontSize }
    },
}


@DragSource('dragElement', boxSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
class DnDElement extends Component {
    constructor (props) {
        super(props);
        this.state = {
            text: this.props.children,
            fontSize: this.props.fontSize,
            editMode: false
        }

        this.edit = this.edit.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.handleCloseEditor = this.handleCloseEditor.bind(this);
    }

    edit () {
        this.setState({ editMode: true })
    }

    handleClickOutside () {
        console.log(123)
    }

    handleCloseEditor () {
        this.setState({ editMode: false })
    }

    render () {
        const {
            hideSourceOnDrag,
            left,
            top,
            connectDragSource,
            isDragging
        } = this.props

        if (isDragging && hideSourceOnDrag) {
            return null
        }

        return connectDragSource(
            <div style={{ fontSize: this.state.fontSize, position: 'absolute', left, top, }} >
               <Popover
                    open={this.state.editMode}
                    onClose={this.handleCloseEditor}
                    anchorEl={this.anchorEl}
                    anchorReference='anchorEl'
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    classes={{
                        paper: 'popover'
                    }}
                >
                    <IconButton color="default" aria-label="bold"><FormatBold/></IconButton>
                    <IconButton color="default" aria-label="italic"><FormatItalic/></IconButton>
                    <TextField
                        onChange={e => this.setState({fontSize: e.target.value})}
                        value={this.state.fontSize}
                        style={{
                            position: 'relative',
                            top: '7px',
                            width: '70px',
                        }}
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <FormatSize color="action"/>
                            </InputAdornment>
                        ),
                        }}
                    />
                </Popover>
                <div 
                    ref={node => this.anchorEl = node}
                    style={{ cursor: 'move', margin: 0, lineHeight: 1, background: this.state.editMode ? 'rgba(255,255,0,0.5)' : null, fontSize: this.state.fontSize }} 
                    onDoubleClick={this.edit}
                >
                    {this.state.text}
                </div>
            </div>
        )
    }
}

export default enhanceWithClickOutside(DnDElement);