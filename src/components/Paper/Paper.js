import React, { Component } from 'react';
import './Paper.sass';

import { Card } from 'material-ui';


const formats = {
    'A3' : 297/420,
    'A4' : 210/297,
    'A5' : 148/210
}

export default class Paper extends Component {
    constructor (props) {
        super (props);
        this.state = {
            format: this.props.format || 'A4',
            orientation: this.props.orientation || 'portrait',
            width: this.props.width || '100%',
            height: 'auto'
        }
    }

    prepareFormat () {
        let height = null;
        

        if (this.state.orientation === 'portrait') {
            height = this.paper.offsetWidth / formats[this.state.format] + 55
        } else {
            height = formats[this.state.format] * this.paper.offsetWidth
        }

        this.setState({ height });
    }

    componentDidMount () {
        this.prepareFormat();
        window.addEventListener('resize', this.prepareFormat.bind(this));
    }


    render () {
        return (
            <div ref={paper => this.paper = paper} className="paper" style={{ height: this.state.height, width: this.state.width }}>
                <div className="paper-wrapper">
                    <Card style={{overflow: 'hidden', height: '100%', width: '100%'}}>
                        {this.props.children}
                    </Card>
                </div>
            </div>
        )
    }
}