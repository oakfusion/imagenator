import React, { Component } from 'react';
import { DropTarget, DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import jsPDF from 'jspdf';
import './App.css';
import Paper from './components/Paper/Paper';
import DnDElement from './components/DnDElement/DnDElement';
import Dropzone from 'react-dropzone';

import { 
  FileUpload,
  Search,
  PictureAsPdf,
  ModeEdit,
  PanTool
 } from '@material-ui/icons';

import { 
  Button,
  AppBar,
  Toolbar,
  Typography,
  CardActions,
  CardContent,
  LinearProgress,
  IconButton,
  Tooltip 
 } from 'material-ui';


const Tesseract = window.Tesseract;

const boxTarget = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    const delta = monitor.getDifferenceFromInitialOffset();
    const left = Math.round(item.left + delta.x);
    const top = Math.round(item.top + delta.y);

    component.moveElement(item.id, left, top)
  }
}

@DragDropContext(HTML5Backend)
@DropTarget('dragElement', boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget()
}))
class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
      progress: {
        status: '',
        progress: 0

      },
      size: {
        width: 0,
        height: 0
      },
      result: null,
      hideSourceOnDrag: true,
      activeDropZone: true,
      mode: 'move'
    }

    this.handleImageChange = this.handleImageChange.bind(this);
    this.calculateRatio = this.calculateRatio.bind(this);
    this.analizeImage = this.analizeImage.bind(this);
    this.createPdf = this.createPdf.bind(this);
    this.openDropzoneOnClick = this.openDropzoneOnClick.bind(this);
    this.test = this.test.bind(this);
  }

  test () {
    console.log(123)
  }

  moveElement(id, left, top) {
    this.setState(
      update(this.state, {
        result: {
          words: {
            [id]: {
              $merge: { 
                baseline: {
                  y0: top / this.state.ratio
                },
                bbox: {
                  x0: left / this.state.ratio
                }
              }
            }
          }
        }
      })
    )
  }

  openDropzoneOnClick () {
    this.setState({ activeDropZone: true }, () => {
      this.dropzone.open()
    })
  }

  handleImageChange (files) {
    let reader = new FileReader();
    let file = files[0];

    reader.onloadend = (event) => {
      let image = new Image();
      image.src = event.target.result;

      image.onload = () => {
        this.setState({
          file,
          imagePreviewUrl: event.target.result,
          progress: {
            status: '',
            progress: 0
    
          },
          size: {
            width: image.naturalWidth,
            height: image.naturalHeight
          },
          result: null,
          activeDropZone: false
        });
      }
    }

    reader.readAsDataURL(file);
  }

  calculateRatio () {
    let imageWidth = this.state.size.width;
    let paperWidth = this.refs.analizePaper.paper.clientWidth - 81;
    let ratio = 1;
    let fontSizeRatio = 3.5;

    if (imageWidth > paperWidth ) {
      ratio = paperWidth / imageWidth;
      fontSizeRatio = 1;
    }

    return {ratio, fontSizeRatio}
  }

  analizeImage (image) {
    Tesseract.recognize(image, {
        lang: 'pol',
        classify_font_name: 'Arial',
        textord_min_linesize: 1
      })
      .progress(p => this.setState({ progress: p }))
      .then(result => {
        let ratio = this.calculateRatio();
        this.setState({ result, ratio: ratio.ratio, fontSizeRatio: ratio.fontSizeRatio });
      })
  }

  createPdf (html) {
    let pdf = new jsPDF({
      unit: 'px',
      format: [1123, 794]
    });

    let pdfRatio = 0.85;

    for (let i = 0; i < html.children.length; i++) {
      let fontSize = html.children[i].style.fontSize.slice(0, -2);
      fontSize = fontSize * pdfRatio
      pdf.setFontSize(Math.round(fontSize));
      pdf.text(html.children[i].innerText, html.children[i].offsetLeft * pdfRatio, html.children[i].offsetTop * pdfRatio);
    }

    pdf.output('dataurlnewwindow');
  }

  componentDidMount () {
    window.onresize = () => {
      this.forceUpdate()
    }
  }

  render() {
    let { imagePreviewUrl, hideSourceOnDrag } = this.state;
    const { connectDropTarget } = this.props;

    return connectDropTarget(
      <div className="App">
        <AppBar position="sticky">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Imagenator
            </Typography>
          </Toolbar>
        </AppBar>
        
        <div className="workspace">
          <Dropzone 
            onDrop={this.handleImageChange} 
            ref={node => this.dropzone = node}
            multiple={false}
            disabled={!this.state.activeDropZone}
            style={{
              height: imagePreviewUrl ? '100%' : 'calc(100vh - 64px)',
              width: '100%',
              border: 'none',
              margin: 0,
              opacity: 1
            }}
            activeStyle={{
              opacity: 0.5
            }}>
            {
              imagePreviewUrl ?
                (
                  <div className="papers-wrapper">
                    <Paper width="50%">
                      <CardActions style={{ borderBottom: '1px dashed #ccc' }}>
                        <Toolbar disableGutters={true} style={{ flex: 1 }}>
                          <Button variant="raised" color="primary" onClick={this.openDropzoneOnClick}>Upload new image <FileUpload style={{ marginLeft: '10px' }}/></Button>
                        </Toolbar>
                      </CardActions>

                      <CardContent style={{ padding: 0, lineHeight: 0, textAlign: 'left' }}>
                        <img ref="analizedImage" className="analized-image" src={imagePreviewUrl} alt="analized file"/>
                      </CardContent>
                    </Paper>

                    <Paper ref="analizePaper" width="50%">
                      <CardActions style={{ borderBottom: '1px dashed #ccc' }}>
                        <Toolbar disableGutters={true} style={{ flex: 1 }}>
                          {
                            this.state.result
                            ? <div style={{ display: 'flex', justifyContent: 'space-between', flex: 1, alignItems: 'center' }}>
                                <Button variant="raised" color="secondary" onClick={() => this.createPdf(this.refs.analizePaperContent)}>create PDF<PictureAsPdf style={{ marginLeft: '10px' }}/></Button>
                                <div>
                                  <Tooltip title="Move mode"><IconButton aria-label="move" color={this.state.mode === 'move' ? 'secondary' : 'default'} onClick={() => this.setState({ mode: 'move' })}><PanTool/></IconButton></Tooltip>
                                  <Tooltip title="Edit mode"><IconButton aria-label="edit" color={this.state.mode === 'edit' ? 'secondary' : 'default'} onClick={() => this.setState({ mode: 'edit' })}><ModeEdit/></IconButton></Tooltip>
                                </div>
                              </div>
                            : <Button variant="raised" color="secondary" onClick={this.analizeImage.bind(null, imagePreviewUrl)}>Analize <Search style={{ marginLeft: '10px' }}/></Button>
                          }
                        </Toolbar>
                      </CardActions>

                      <CardContent style={{ padding: 0, lineHeight: 0, textAlign: 'left', width: '100%', height: '100%' }}>
                        {
                          this.state.result
                          ? <div ref="analizePaperContent" style={{position: 'relative', width: '100%', height: '100%'}}>
                              {
                                this.state.result.words.map((word, index) => {
                                  return <DnDElement 
                                          key={index} 
                                          id={index} 
                                          fontSize={word.font_size * this.state.fontSizeRatio}  
                                          top={word.baseline.y0 * this.state.ratio} 
                                          left={word.bbox.x0 * this.state.ratio} 
                                          hideSourceOnDrag={hideSourceOnDrag}
                                          handleEdit={this.test}
                                          >
                                            {word.text}
                                        </DnDElement>
                                })
                              }
                            </div>
                          : <div>
                              {
                                this.state.progress.status
                                ? <div style={{ textAlign: 'center' }}>
                                    <LinearProgress variant="determinate" value={(this.state.progress.progress * 100)  } />
                                    <Typography variant="title" color="primary">{this.state.progress.status}</Typography>
                                  </div>
                                : null
                              }
                            </div>
                        }
                      </CardContent>
                    </Paper>
                  </div>
                )
              : <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', padding: '100px 160px', border: '2px dashed #aaa', borderRadius: '6px', cursor: 'pointer', opacity: 'inherit' }}>
                  <Typography variant="title" color="default">Drop your image here!</Typography>
                  <Typography variant="subheading" color="default">Or Click to upload</Typography>
                </div>
            }
          </Dropzone>
        </div>
      </div>
    );
  }
}

export default App;
